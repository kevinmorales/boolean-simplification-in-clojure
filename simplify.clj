(ns main.core)
;;*********************Test Cases************************
(def andtest '(and true true x))
(def ortest '(or x true))
(def ortest2 '(or x false))
(def notTest '(not x))
(def test1 '(and x (or x (and y (not z)))))
(def test2 '(and x (or x (and y (not z)))))
(def test3 '(not (and y (not x))))
(def test4 '(not (and x y (not z))))
;;*******************************************************


;;**************************************************************************
;; Description: Write a set of Clojure functions that perform symbolic 
;;              simplification and evaluation of boolean expressions using
;;              and, or, and not. 
;;              Here is the main method that will run the test cases in the 
;;              order specified in the test cases given. 
;;              Note: I Java-fied the format of the Clojure code because 
;;                    it is more readable for me like this.
;;**************************************************************************

(defn -main [& args] 

  ;;Run Test Cases
  (println (evalexp ortest '{x false}))
  (println (evalexp andtest '{x false}))
  (println (evalexp notTest '{x false}))
  (println "Test cases below")
  (println (evalexp test1 '{x false z true}))
  (println (evalexp test2 '{z false}))
  (println (evalexp test3 '{y true}))
  (println (evalexp test4 '{x true}))

);;END MAIN

;;**************************************************************************
;; Function: and-simplify
;; Description: This function takes arguments that will be 
;; evaluated based on the 'and' operator. This function expects
;; one or many arguments to be passed to it. 
;;
;; Note 1. Let statement: Here we pass the arguments from the parameters
;;         to this let statement to be evaluated and the output will be
;;         a lazy sequence with no true statements in it.
;;         The function arguments are parsed for 'true' statements. All
;;         non-true statements are stored in a lazy sequence.
;;         Finally, all duplicates in the lazy sequence are removed with
;;         the distinct function call.  
;; Note 2. Conditional statement to evaluate the non-true arguments that
;;         were filtered in step 1. 
;;         IF there are any false arguments in the sequence
;;            the entire statement is false, return false
;;         ELSE IF there is only one argument in the sequence
;;            this argument will just be the 'and', return true 
;;         ELSE IF there are two arguments in the sequence
;;            this is something like (and true) or (and false)
;;            return the second element of this sequence
;;         END CONDITIONAL 
;;**************************************************************************

(defn and-simplify
  [args]
  ;;See Note 1
  (let [filteredArgs ;;4. Store filtered and distinct args in this list
    (distinct ;;3. Remove duplicate list elements
      (filter (fn [filtered-seq] ;;2.Store filtered values in this list
          (not(true? filtered-seq)))args))] ;;1. Filter 'true args from args list
    ;(println noTrue) Uncomment to test the filter
    ;;See Note 2
    ;Count of elements in filteredArgs 
    (def argCount (count filteredArgs))

    ;;Conditional to check the filtered sequence for truthyness
    (cond
      ;; IF any arguments are false, then the whole argument is false
      (some false? filteredArgs)
        ;;RETURN FALSE
        false
      ;;ELSE IF there is only one argument in the sequence
      (= argCount 1) 
        ;;RETURN TRUE
        true  
      ;;ELSE IF there are two arguments in the sequence
      (= argCount 2)
        ;;RETURN the second indexed element from the sequence
        (nth filteredArgs 1)
      ;;ELSE there is not a way to evaluate the arguments
      :else
        ;;RETURN the whole true-filtered list
        filteredArgs
    );;END COND
  );;END LET
);;END and-simplify

;;**************************************************************************
;; Function: or-simplify
;; Description: This function takes arguments that will be 
;; evaluated based on the 'or' operator. This function expects
;; one or many arguments to be passed to it. 
;; Note 1. Let statement: Here we pass the arguments from the parameters
;;         to this let statement to be evaluated and the output will be
;;         a lazy sequence with no true statements in it.
;;         The function arguments are parsed for 'false' statements. All
;;         non-false statements are stored in a lazy sequence.
;;         Finally, all duplicates in the lazy sequence are removed with
;;         the distinct function call.  
;; Note 2. Conditional statement to evaluate the non-falsy arguments that
;;         were filtered in step 1. 
;;         IF there are any true arguments in the sequence
;;            the entire statement is true, return true
;;         ELSE IF there is only one argument in the sequence
;;            this argument will just be the 'or', return false 
;;         ELSE IF there are two arguments in the sequence
;;            this is something like (or true) or (or false) or (or x)
;;            return the second element of this sequence
;;         END CONDITIONAL 
;;**************************************************************************

(defn or-simplify
  [args]
  ;;See Note 1
  (let 
    [filteredArgs ;;3. Store filtered and distinct args in filteredArgs list 
      (distinct ;;2. Remove any duplicate args in filtered list
        (filter ;;1. Filter out the false values from the args list
          (fn [filtered-seq] (not(false? filtered-seq)))args))]
    ;;filteredArgs is a sequence with no 'false' elements in it
    ;;See Note 2
    (def argCount (count filteredArgs))

    ;;Conditional to check the filtered sequence for truthyness 
    (cond
      ;;IF there is one 'true' argument
      (some true? filteredArgs)
        ;;RETURN TRUE
        true
      ;;ELSE IF there is only one element in the sequence
      (= argCount 1)
        ;;RETURN FALSE
        false
      ;;ELSE IF there are two elements in the sequence                     
      (= argCount 2)
        ;;RETURN the second element in the sequence
        (nth filteredArgs 1)
      ;;ELSE there is not a way to evaluate the arguments
      :else 
        ;;RETURN the entire non-false sequence
        filteredArgs
      );;END COND
    );;END LET
  );;END or-simplify 

;;**************************************************************************
;; Function: not-simplify
;; Description: This function takes arguments that will be 
;; evaluated based on the 'not' operator. This function expects
;; one argument to be passed to it, but can handle multiple arguments by
;; calling the simplify function to reduce it further.  
;;**************************************************************************

(defn not-simplify
  [args]

  ;;local variables
  (def lastArg (last args))
  (def nested? (seq? lastArg))

  ;;See Note 1
  ;;BEGIN CONDITIONAL
  (cond
    ;;IF a true argument is in the args list
    (some true? args)
        ;;RETURN FALSE
        false 

    ;;ELSE IF a false argument is in the args list                                
    (some false? args)
        ;;RETURN TRUE
        true
      
    ;;ELSE IF (The last argument is a sequence AND the first element of the last sequence is 'not')
    (and nested? (= 'not (first lastArg)))
        ;;RETURN the last element in the arguments sequence that is nested in the function arguments
        (last lastArg)

    ;;ELSE IF (The last argument is a sequence AND the first element of the last sequence is 'not')
    (and nested? (= 'and (first lastArg)))
        ;;Simplify the sequence within the sequence and RETURN the simplification
        (simplify
          ;;Replace the 'and' in the sequence with 'or', then map not values to the rest of the list
          ;;(cons 'or (map-not (rest lastArg))))
          (deMorgans 'or (rest lastArg)))

    ;;ELSE IF (The last argument is a sequence AND the first element of the last sequence is 'or)
    (and nested? (= 'or (first(lastArg))))
      ;;Simplify the sequence within the sequence and RETURN the simplification
      (simplify
        ;;Replace the 'or' in the sequence with 'and', then map not values to the rest of the list
        (deMorgans 'and (rest lastArg)))

    ;;ELSE the argument could not be resolved, likely unmapped variable
    :else 
      ;;RETURN the arguments as they were passed in 
      args

  );;END CONDITIONAL
);;END not-simplify

;;**************************************************************************
;; Description: This function performs DeMorgan's law on a a set of 
;; arguments. A boolean operator is needed to complete the transformation.
;; The boolean operator is just appended to the front of the argument list 
;; after it is evaluated. 
;;
;; DeMorgan's examples:
;;  not (A or B) = not A and not B
;;  not (A and B) = not A or not B.
;;**************************************************************************

(defn deMorgans

  [bool args]
  ;;Negate all elements in the args 
  (def negArgs (map #(list 'not %) args))
  ;;Append the boolean operator to the beginning of the sequence
  (cons bool negArgs)

);;END deMorgans


;;Function to search if a binding value and list element match
(defn search
  [list-element inMap]
  ;;RETURN the matching binding value if it is present in the list, if not return the list element
  (get inMap list-element list-element))

;;**************************************************************************
;; Function: bind-values
;; Description: This function takes a list and map as parameters. It's 
;; purpose is to map matching key-value pairs from the map into the 
;; given list. If a list element is a nested sequence, the function will
;; perform a recursive call to map values to that nested sequence. 
;; The end result is a list whose elements were substituted with the values
;; in the map. 
;;*************************************************************************

(defn bind-values
  
  [inList inMap]
  (map (fn [list-element]
    ;;IF the list element is a sequence within the list
    (if (seq? list-element)
      ;;Bind the values of the variables to the values in the list
      (bind-values list-element inMap)
      ;;Retrieve the matching binding value for the list element in the map,if not present return the list element
      (search list-element inMap)
    );;END IF
  );;END anonymous function
  inList);;Map result of anonymous function to the list passed in
);;END substitute function

;;**************************************************************************
;; Function: evalexp
;; Description: This function is given a list of expressions and a map with 
;; bindings that resolve the symbols x, y, z to true or false. The bindings 
;; may not resolve all the symbols, so there could be expressions with unknown
;; bindings in the final list. 
;;*************************************************************************

(defn evalexp
  [exp bindings]
  ;;Call simplify function with the list after it is substituted with its bindings
  (simplify
    (bind-values exp bindings))
);;END evalexp

;;**************************************************************************
;; Function: simplify
;; Description: This function takes arguments and finds the appropriate 
;; function to evaluate and simplify the argument
;; There is a recursive call that will recur unit the argument is no 
;; longer a sequence and can then be simplified further.
;;*************************************************************************

(defn simplify
  [args]
  ;;BEGIN OUTER CONDITIONAL
  (cond
    ;;IF the arguments are a sequence
    (seq? args) 
      ;Recursively call the simplify function until we get to a simplified argument
      (let [simplifiedArgs (map simplify args)]
      ;;BEGIN INNER CONDITIONAL
      (cond
        ;;IF the first argument is 'and;
        (= (first args) 'and)
          (and-simplify simplifiedArgs)
        
        ;;ELSE IF the first argument is 'or'
        (= (first args) 'or)
          (or-simplify simplifiedArgs)

        ;;ELSE IF the first argument is 'and'
        (= (first args) 'not)
          (not-simplify simplifiedArgs)))
    
    ;;ELSE the argument is not a sequence
    :else 
      ;;RETURN the arguments as they were passed in 
      args

  );;END CONDITIONAL
);;END simplify
